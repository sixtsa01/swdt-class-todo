#!/bin/bash
rm -rf "swit-class-todo"
git clone https://sixtsa01@bitbucket.org/sixtsa01/swdt-class-todo.git
cd swdt-class-todo
find . -name "*.pyc" -exec rm {} +
find . -name "__pycache__" -exec rm -rf {} +
find . -name "*.class" -exec rm {} +
find . -name "*.out" -exec rm {} +
find . -name "*.txt" -exec rm {} +
git add .
git commit -m "Cleaned this respository"
git push origin master
cd ..
cp 'exercise10.sh' swdt-class-todo
cd swdt-class-todo
git add 'exercise10.sh'
git commit -m "Script to clean repository"
git push
